/* eslint no-undef: 0 */
const GShellExtension = require('../lib/GShellExtension');
const GSettings = require('../lib/GSettings');



const GS_SCHEMA = new GSettings('org.gnome.shell'),
      EXTENSION_KEY='',
      EXTENSION_PATH='./fixtures/node-gshellextension-test';


let assert = require('assert'),
    should = require('should'),
    path = require('path');




var testExtensionConfig = require(EXTENSION_PATH+'/metadata.json'),
  extensionKey = testExtensionConfig.uuid;




describe('Module', function () {
  let app = require('../');
  describe('#init()', function () {
    it('should create a instance of GShellExtension', function () {
      let ext = app(testExtensionConfig);
      ext.should.be.an.instanceOf(GShellExtension);
    });
  });    
});
