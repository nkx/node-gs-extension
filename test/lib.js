/* eslint no-undef: 0 */
const GShellExtension = require('../lib/GShellExtension');
const GSettings = require('../lib/GSettings');
const GS_SCHEMA = new GSettings('org.gnome.shell'),
      EXTENSION_KEY='',
      EXTENSION_PATH='./fixtures/node-gshellextension-test',
      LOCAL_EXTENSION_DIR = process.env.HOME + '/.local/share/gnome-shell/extensions';

let assert = require('assert'),
    should = require('should'),
    path = require('path'),
    fs = require('fs');

var testExtensionConfig = require(EXTENSION_PATH+'/metadata.json'),
  extensionKey = testExtensionConfig.uuid;




describe('lib/GShellExtension', function () {
  let extension = new GShellExtension(testExtensionConfig, path.resolve('./test/',EXTENSION_PATH)),
      enabledExtensionKey = GS_SCHEMA.findKeyById('enabled-extensions');
  //GShellExtension.link()
  describe('#enable()', function () {
    it('should be in the schema.enabled-extension list after enabling', function () {
      extension.enable();
      enabledExtensionKey.getValue().should.containEql(extensionKey);
    });
  });
  describe('#disable()', function () {
    it('schema enabled-extension should not contain extension after disabling', function () {
      extension.disable();
      enabledExtensionKey.getValue().should.not.containEql(extensionKey);
    });
    
    
    
  });
  describe('#link()', function () {
    it('extension should be linked to the installation directory', function () {
      extension.link();
      fs.existsSync(LOCAL_EXTENSION_DIR+'/'+extensionKey).should.be.ok();
    });
  });
  describe('#unlink()', function () {
    it('extension should not exist with in the installation directory', function () {
      extension.unlink();
      fs.existsSync(LOCAL_EXTENSION_DIR+'/'+extensionKey).should.be.false();
    });
  });
  describe('#install()', function () {
    it('extension should exist and should be enabled', function () {
      extension.install();
      fs.existsSync(LOCAL_EXTENSION_DIR+'/'+extensionKey).should.be.true();
    });
  });
  describe('#remove()', function () {
    it('extension should not exist and should be disabled', function () {
      extension.remove();
      fs.existsSync(LOCAL_EXTENSION_DIR+'/'+extensionKey).should.be.false();
    });
    
  });
    
});