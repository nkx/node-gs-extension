/**
 * @module GnomeShellExtension
 * @property {GSettings} GSettings
 */
/* eslint no-console: 1 */
'use strict';
const shelljs = require('shelljs'),
    fs = require('fs'),
    path = require('path'),
    GShellExtension = require('./lib/GShellExtension');

/**
 * __creates a GShellExtension Instance__
 * 
 * if you want to link the extension or create a new one,
 * you should define sourceDir.
 * @param   {string|object}   uuid      the uuid or an object containing the uuid
 * @param   {Object=}         opt       the meta data of the extension
 * @param   {String=}         sourceDir source path of an external extension. 
 * @returns {GShellExtension} new instance
 */
module.exports = function (uuid, opt = null, sourceDir = null) {
    return new GShellExtension(uuid, opt, sourceDir);
};

/**
 * reload gnome-shell 
 * @function reload
 */
module.exports.reload = function () {
    shelljs.exec('gnome-shell --replace &', function () {
        
    });
};
/**
 * @var {GSettings}
 */
module.exports.GSettings = require('./lib/GSettings.js');
module.exports.GShellExtension = require('./lib/GShellExtension.js');
