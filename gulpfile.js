'use strict';
const path = require('path');
const gulp = require('gulp');
const gutil = require('gulp-util');
const eslint = require('gulp-eslint');
const nodemon = require('gulp-nodemon');
const mocha = require('gulp-mocha');
const shelljs = require('shelljs');
const exec = require('gulp-exec');
const jsdoc = require('gulp-jsdoc3');
const babel = require('gulp-babel');

const connect = require('gulp-connect');

/*
const logger = require(process.cwd()+'/../../../Node/advanced-logger'),
*/
let ok = gutil.log.bind(gutil.log),
    fail = gutil.log.bind(gutil.log),
    log = gutil.log.bind(gutil.log);



let JS_FILES = ['**/*.js', '!node_modules/**', '!docs/**'];

//console.log(logger.ok)


function lintFile(file) {
    ok('Linting File', '@magenta');
    return gulp.src(file)
        .pipe(eslint())
    // eslint.format() outputs the lint results to the console. 
    // Alternatively use eslint.formatEach() (see Docs). 
        .pipe(eslint.format());
    // To have the process exit with an error code (1) on 
    // lint error, return the stream and pipe to failAfterError last. 
    //.pipe(eslint.failAfterError());

}

gulp.task('lint', () => {
    // ESLint ignores files with "node_modules" paths.
    // So, it's best to have gulp ignore the directory as well.
    // Also, Be sure to return the stream from the task;
    // Otherwise, the task may end before the stream has finished.

    return gulp.src(JS_FILES)
    // eslint() attaches the lint output to the "eslint" property 
    // of the file object so it can be used by other modules. 
        .pipe(eslint())
    // eslint.format() outputs the lint results to the console. 
    // Alternatively use eslint.formatEach() (see Docs). 
        .pipe(eslint.format())
    // To have the process exit with an error code (1) on 
    // lint error, return the stream and pipe to failAfterError last. 
        .pipe(eslint.failAfterError());
});


gulp.task('monit', () => {
    nodemon({
        ext: 'js',
        //stdout:   false,
        watch:    ['lib/*.js','test/*.js','index.js'],
        env: {
            'NODE_ENV': 'development'
        },
        tasks: function (changedFiles) {

            let tasks = [];
            if (!changedFiles) return tasks;
            changedFiles.forEach(function (file) {
                if (~file.indexOf('docs/')) { // ignore all changes with in the generated jsdoc directory
                    return;
                }
                if (file.match(/test\/.*\.js/)!==null) {
                    gulp.src(file)
                        .pipe(mocha({
                        reporter: 'list' || 'spec' || 'nyan'
                    }));
                } else if (path.extname(file) === '.js' && !~tasks.indexOf('lint')) {
                  lintFile(file);
                  if (file.match(/lib\/.*\.js/)!==null) {
                    gulp.src("test/lib.js")
                        .pipe(mocha({
                        reporter: 'list' || 'spec' || 'nyan'
                    }));
                  }
                }
              

                //if (path.extname(file) === '.js' && !~tasks.indexOf('lint')) tasks.push('lint')
                //if (path.extname(file) === '.css' && !~tasks.indexOf('cssmin')) tasks.push('cssmin')
            });
            return tasks;
        }
    });
});

/*
gulp.task('dev', () => {
    gulp.watch(JS_FILES, function (ev) {
        try {
            if (ev.type === 'added' || ev.type === 'changed') {
                lintFile(ev.path);
            }
        } catch (e) {
            fail('watch: linting failed for ' + ev.path);
            fail(e);
        }
    });
});

*/



gulp.task('gulpfile', () => {
    lintFile('gulpfile.js');
});

gulp.task('default', ['lint'], function () {
    // This will only run if the lint task is successful... 
});


gulp.task('connect', function () {
    connect.server({
        root: 'docs/gen',
        livereload: true,
        port: 8000
    });
});

gulp.task('html', function () {
    gulp.src('./docs/gen/index.html')
        .pipe(connect.reload());
});

let webkitRunning = false;

gulp.task('webkit', function () {
    if (webkitRunning) {
        return;
    }
    shelljs.exec('chromium-browser --app=http://localhost:8000', function () {
        webkitRunning = true;
        connect.reload();
    });
});

gulp.task('watch', function () {
    gulp.watch(['index.js', 'lib/*.js'], ['jsdoc']);
    gulp.watch(['./docs/gen/*.html'], ['html']);
});

gulp.task('jsdoc', function () {
    gulp.src(['index.js','lib/*.js'])
        .pipe(jsdoc());
    gulp.start('html');
});


gulp.task('dev', ['connect', 'webkit', 'watch']);


gulp.task('build',function(){
	gulp.src(JS_FILES)
		.pipe(babel({
			presets: ['env']
		}))
		.pipe(gulp.dest('dist'));
});

gulp.task('default', ['lint','build']);
