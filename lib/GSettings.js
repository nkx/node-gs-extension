

const spawnSync = require('child_process').spawnSync,
      fs = require('fs'),
      path = require('path'),
      shelljs = require('shelljs'),
      _ = require('lodash'),
      GSettingsWrapper = require('node-gsettings-wrapper');



function parseKeySetValue(v) {
    if(v instanceof Object) {
        v = JSON.stringify(v);
    } else if('toString' in v) {
        v = v.toString();
    }
    return v;
}
class Key extends GSettingsWrapper.Key {
    static findById(schemaId, keyId) {
        if (!Key.exists(schemaId, keyId)) {
            return null;
        }
        return new Key(new GSettings(schemaId), keyId);
    }

    setValue(value) {
        const process = spawnSync("gsettings",
                                  ["set", this._schema.getId(), this.getId(), parseKeySetValue(value)]);
        if(process.stderr.toString()) {
            throw new Error("Error:"+process.stderr.toString());
        }
        return process.stdout;
    }
}

/**
 * Extends the {@link https://www.npmjs.com/package/node-gsettings-wrapper GSettingsWrapper}, with some helpers.
 *  
 * @class 
 */
class GSettings extends GSettingsWrapper.Schema {
    /**
     * @param {string} id Schema Key
     * @constructor
     */
  
    constructor(id) {
        super(id);
    }
    containsKey(keyId) {
        return Key.exists(this._id, keyId);
    }

    findKeyById(keyId) {
        return Key.findById(this._id, keyId);
    }
    /**
     * get/set property of Schema
     * @param   {string} key   property key
     * @param   {mixed}  value optional. if given, the property gets setted/updated.
     * @returns {mixed}   the founded property value if argument value not passed
     */
    prop(keyId, value) {
        if(!this.containsKey(keyId)) {
            throw new Error('key of schema '+this._id+' not found. '+keyId);
        }
        let key = this.findKeyById(keyId);
        if(value===undefined){
            return key.getValue();
        }
        key.setValue(value);

    }

}


module.exports = GSettings;