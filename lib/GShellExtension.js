var GSettings = require('./GSettings'),
  fs = require('fs'),
  path = require('path'),
  shelljs = require('shelljs'),
  uniq = require('lodash.uniq'),
  _ = require('lodash');


const LOCAL_EXTENSION_DIR = process.env.HOME + '/.local/share/gnome-shell/extensions';

/**
 * GShellExtensionHandler
 * Helper Class to manage a single Extension
 * 
 * @class 
 */
class GShellExtension {
  /**
   * @param {string} uuid      unique extension key
   * @param {object} opt    meta   data object (uuid, description e.g.)
   * @param {string} sourceDir optional directory where extension is located
   * @constructor
   */
  constructor(uuid, opt = null, sourceDir = null) {
    if(typeof(opt)==='string') {
      sourceDir=opt;
      opt=null;
    }
    if (uuid instanceof Object) {
      opt = uuid;
      uuid = opt.uuid;
    }
    this.uuid = uuid;
    this.conf = (opt || {});
    this.source = sourceDir;
    /**
     * @property {schema} gsettings schema
     * @public
     */
    this.schema = new GSettings('org.gnome.shell');
  }
  /**
   * Gnome Shell Settings<br />
   * Get a Property from "org.gnome.shell"
   * @param   {string} key Property Key
   * @returns {object} Property Object.<br />Use .getValue to actually get the value of the property
   */
  static schemaProperty(key) {
    return new GSettings.Key.findById('org.gnome.shell', key);
  }
  /**
   * enables extension
   */
  enable() {
    let list = this.schema.prop('enabled-extensions');
    if (list) {
      list = uniq(list.concat(this.uuid));
      this.schema.prop('enabled-extensions', list);
    }
  }
  /**
   * disables extension
   */
  disable() {
    let list = this.schema.prop('enabled-extensions');
    if (list) {
      list = _.without(list, this.uuid);
      this.schema.prop('enabled-extensions', list);
    }
  }
  /**
   * extension installation path
   * @private
   * @returns {string} the local installation path for gnome extensions
   */
  _installPath() {
    return path.join(LOCAL_EXTENSION_DIR, this.uuid);
  }
  /**
   * check if source exists
   * otherwise an Error will be thrown
   * @private
   */
  _validateSource() {
    if (!this.source) {
      throw new Error('this.source is not defined!');
    }
    let sourceStat=fs.lstatSync(this.source);
    if(sourceStat.isDirectory() || sourceStat.isSymbolicLink()) {
      return true;
    }
    throw new Error('no valid source: '+sourceStat);
  }
  /**
   * install extension
   * copies the extension into the installation directory and enables it 
   */
  install() {
    this._validateSource();
    let destination = this._installPath();
    if(destination===this.source) {
      this.enable();
      return;
    }
    // remove allready present extension from the installation dir
    if (shelljs.test('-d', destination)) {
      this.remove();
    }
    // create extension directory
    if (!shelljs.test('-d', destination)) {
      shelljs.mkdir(destination);
    }
    // copy all files from source
    if (this.source && typeof (this.source) === 'string') {
      shelljs.cp('-f', this.source + '/*', destination);
    }
    this.enable();
  }
  /**
   * removes the extension
   */
  remove() {
    
    let destination = this._installPath();
    this.disable(this.uuid);
    if (shelljs.test('-L', destination)) {
      this.unlink();
    } else if (shelljs.test('-d', destination)) {
      shelljs.rm('-r', destination);
    }
  }
  /**
   * removes the extension simlink 
   */
  unlink() {
    let symlink = this._installPath();
    if (shelljs.test('-L', symlink)) {
      fs.unlinkSync(symlink);
    } else if (shelljs.test('-D', symlink)) {
      throw new Error('could not unlink it. extension allready exists and is an directory!');
    }
  }
  /**
   * Link source directry to final extensions directory
   * mainly used for developing..
   */
  link() {
    this._validateSource();
    
    let target = this._installPath();
    if (shelljs.test('-L', target)) {
      fs.unlinkSync(target);
    } else if (shelljs.test('-d', target)) {
      throw new Error('Target "' + target + '" allready exists.');
    }
    fs.symlinkSync(path.resolve(this.source), target);
  }

}
module.exports = GShellExtension;